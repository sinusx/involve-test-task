FROM debian-python3.7.7

# setup linux
ENV TZ=Europe/Kiev
ENV LANG="en_US.UTF-8"
ENV LC_ALL="en_US.UTF-8"
ENV LC_LANG="en_US.UTF-8"
ENV PYTHONIOENCODING="UTF-8"

# setup application
COPY ./requirements.txt /tmp
RUN pip install -r /tmp/requirements.txt
RUN mkdir /opt/app/ /var/media
WORKDIR /opt/app

COPY ./src/ /opt/app/

# setup ci/cd
COPY ./ci/config.toml /etc/gitlab-runner/config.toml

ENTRYPOINT /opt/app/scripts/apprun.sh