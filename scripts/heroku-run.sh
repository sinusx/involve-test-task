#!/bin/bash

cd /app/src
ENV_FILE="/app/heroku.env"
export $(cat ${ENV_FILE} | xargs)
if [[ -f "./migrations" ]]; then
  flask db upgrade
else
  sh scripts/dbinit.sh
fi
gunicorn "flaskapp:create_app()"