#!/bin/bash

export FLASK_APP=/home/sinusx/python/test_involve/src/flaskapp
if [ "$1" != "" ]
then
  flask db migrate -m "$1" &&
  flask db upgrade
fi