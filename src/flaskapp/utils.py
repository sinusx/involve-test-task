from flaskapp.config import Config

from hashlib import sha256


def get_sign(filtered_data):
    values = [str(value) for key, value in sorted(filtered_data.items())]
    string = ':'.join(values) + Config.SIGN_SECRET_KEY
    return sha256(string.encode()).hexdigest()
