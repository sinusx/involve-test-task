from flaskapp.db import db
from flaskapp.models.base import BaseModel
from datetime import datetime


class LogModel(db.Model, BaseModel):
    __tablename__ = 'log'

    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Numeric(12, 2))
    currency = db.Column(db.Integer)
    description = db.Column(db.Text)
    shop_order_id = db.Column(db.Integer)
    payment_id = db.Column(db.Integer)
    status_code = db.Column(db.Integer)
    result = db.Column(db.Text)
    error = db.Column(db.Integer)
    message = db.Column(db.Text)
    timestamp = db.Column(db.DateTime, default=datetime.now)

    def __init__(self, props=None):
        if props:
            initable = ['amount', 'currency', 'description', 'shop_order_id']
            for key in initable:
                if props.get(key):
                    self.__setattr__(key, props.get(key))
