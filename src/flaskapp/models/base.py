from flaskapp.db import db


class BaseModel(object):
    # base operations with model

    @classmethod
    def get_all(cls):
        return cls.query.all()

    @classmethod
    def get_item_by_id(cls, _id, session=None):
        if session:
            item = session.query(cls).filter_by(id=_id).first()
        else:
            item = cls.query.filter_by(id=_id).first()
        return item

    def add_to_save_to_db(self):
        try:
            db.session.add(self)
            db.session.flush()
            return True
        except Exception as e:
            print(e)
            return False

    def save_to_db(self):
        try:
            db.session.add(self)
            db.session.commit()
            return True
        except Exception as e:
            print(e)
            return False

    def add_to_delete_from_db(self):
        try:
            db.session.delete(self)
            db.session.flush()
            return True
        except Exception as e:
            print(e)
            return False

    def delete_from_db(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except Exception as e:
            print(e)
            return False
