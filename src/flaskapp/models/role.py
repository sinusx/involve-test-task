from flaskapp.db import db
from flaskapp.models.base import BaseModel
from flask_security import RoleMixin


class RoleModel(db.Model, BaseModel, RoleMixin):
    __tablename__ = 'role'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(15), unique=True)
    description = db.Column(db.String(255))

    def __init__(self, props=None):
        if props:
            self.__dict__.update(props)

    @staticmethod
    def get_role_by_id(_id):
        return RoleModel.query.filter_by(id=_id).first()

    @staticmethod
    def get_role_by_name(name):
        return RoleModel.query.filter_by(name=name).first()

    def json(self):
        return {'id': self.id, 'name': self.name}
