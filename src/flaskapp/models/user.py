from flaskapp.models.base import BaseModel
from flaskapp.db import db
from flask_security import UserMixin

roles_users = db.Table(
    'roles_users',
    db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
    db.Column('role_id', db.Integer(), db.ForeignKey('role.id'))
)


class UserModel(db.Model, BaseModel, UserMixin):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship(
        'RoleModel', secondary=roles_users,
        backref=db.backref('users', lazy='dynamic')
    )

    def __init__(self, props=None):
        if props:
            self.__dict__.update(props)

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()
