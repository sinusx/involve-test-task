from wtforms import Form, StringField, HiddenField, SelectField, TextAreaField, validators
from flaskapp.config import currencies


class PayMethodSelector(Form):
    amount = StringField(
        label='Сумма оплаты: ',
        validators=[validators.DataRequired()],
    )
    currency = SelectField(
        label='Валюта: ',
        choices=currencies,
        validators=[validators.DataRequired()],
    )
    description = TextAreaField(
        label='Описание товара: ',
        validators=[validators.DataRequired()],
    )

    shop_order_id = HiddenField()
