import os

from flask import Flask, request, render_template
from flaskapp.config import Config
from flaskapp.db import db
from flaskapp.models.db_log import LogModel
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView

from flask_migrate import Migrate

from flaskapp.forms import PayMethodSelector
from flaskapp.payment_processor import EuroPayProcessor, USDPayProcessor, RUBPayProcessor

import logging
from logging.handlers import RotatingFileHandler


def create_app(special_config=None):

    app = Flask(__name__)
    app.config.from_object(special_config or Config)

    db.init_app(app)

    migrate = Migrate()
    migrate.init_app(app, db)

    admin = Admin(app, name='admin', template_mode='bootstrap3')
    admin.add_view(ModelView(LogModel, db.session))

    if not os.path.exists('logs'):
        os.mkdir('logs')

    file_handler = RotatingFileHandler('logs/pay-service.log', maxBytes=10240, backupCount=10)
    file_handler.setFormatter(
        logging.Formatter('%(asctime)s %(levelname)s %(funcName)s(%(lineno)d) %(message)s', '%Y.%m.%d %H:%M:%S')
    )
    app.logger.addHandler(file_handler)
    app.logger.setLevel(logging.INFO)

    @app.route('/')
    def get():
        # todo: generate fake data
        form = PayMethodSelector()
        form.amount.data = '100.00'
        form.description.data = 'test'
        form.shop_order_id.data = '4126'
        return render_template('choose-pay-method.html', form=form)

    @app.route('/action_pay', methods=['POST'])
    def process_pay_form():
        form = PayMethodSelector(request.form)
        if form.validate():
            currency = form.currency.data
            if currency == '978':
                response = EuroPayProcessor().process_data(form.data)
                return response
            elif currency == '840':
                response = USDPayProcessor().process_data(form.data)
                return response
            elif currency == '643':
                response = RUBPayProcessor().process_data(form.data)
                return response
            else:
                errors = ['Wrong currency']
                render_template('error.html', errors=errors), 400
        else:
            errors = []
            for item in form.data:
                item_errors = item.get('errors')
                if item_errors:
                    errors.append(item_errors)
            return render_template('error.html', errors=errors), 400

    return app

