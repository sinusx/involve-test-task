import os


currencies = [
    ('978', 'EUR'),
    ('643', 'RUB'),
    ('840', 'USD'),
]


class Errors(object):
    INTERNAL_ERROR = 'Возникла ошибка в работе сервиса. Повторите действие позже или обратитесь в техподдержку.'
    EXTERNAL_ERROR = 'Возникла ошибка платежной системы. Повторите действие позже или обратитесь в техподдержку.'
    DB_ERROR = 'DB error occurred: %s'


class Config(object):
    SHOP_ID = os.environ.get('SHOP_ID')
    SIGN_SECRET_KEY = os.environ.get('SIGN_SECRET_KEY')

    # SQL-Alchemy config
    SESSION_TYPE = 'sqlalchemy'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///sqlite.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ENGINE_OPTIONS = {'echo': False}
    FLASK_ADMIN_SWATCH = 'slate'

    DEBUG = False


class TestConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    SQLALCHEMY_ECHO = True
    DEBUG = True
