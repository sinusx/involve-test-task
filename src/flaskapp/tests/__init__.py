from werkzeug.serving import make_server
import threading
from flaskapp import create_app
from flaskapp.config import TestConfig


class ServerThread(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        app = create_app(TestConfig)

        self.srv = make_server('localhost', 5001, app)
        self.ctx = app.app_context()
        self.ctx.push()

    def run(self):
        self.srv.serve_forever()

    def shutdown(self):
        self.srv.shutdown()


def start_server(server):
    server.start()


def stop_server(server):
    server.shutdown()
