import os
import unittest
from flaskapp import db
from flaskapp.tests import ServerThread, start_server, stop_server
server = None


class BaseTestClass(unittest.TestCase):
    jwt_token = None

    @classmethod
    def setUpClass(cls):
        global server

        # run server
        server = ServerThread()
        start_server(server)

    @classmethod
    def tearDownClass(cls):
        stop_server(server)

    def setUp(self):
        db.drop_all()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
