from flaskapp.models.db_log import LogModel
from flaskapp.tests.models import BaseTestClass
from datetime import datetime


class TestLogModel(BaseTestClass):

    def test_schema(self):
        now = datetime.now()

        log = LogModel()
        log.amount = 100.00
        log.currency = 643
        log.description = 'test'
        log.shop_order_id = 1234
        log.payment_id = 1
        log.status_code = '200'
        log.result = 'ok'
        log.error = 0
        log.message = 'test'
        log.timestamp = now
        result = log.save_to_db()

        self.assertEqual(True, result)
