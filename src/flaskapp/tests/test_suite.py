import unittest

# import test modules
from flaskapp.tests.models.db_log import TestLogModel


loader = unittest.TestLoader()
suite = unittest.TestSuite()

# tests for the test suite
suite.addTests(loader.loadTestsFromTestCase(TestLogModel))


# initialize a runner, pass it suite and run it
runner = unittest.TextTestRunner(verbosity=3)
result = runner.run(suite)

