from flask import render_template, redirect, current_app
from flaskapp.utils import get_sign
from flaskapp.config import Config, Errors

from flaskapp.models.db_log import LogModel

import json
import requests


class EuroPayProcessor(object):

    @staticmethod
    def process_data(data):
        url = 'https://pay.piastrix.com/ru/pay'
        form = {
            'amount': data.get('amount'),
            'currency': data.get('currency'),
            'shop_id': Config.SHOP_ID,
            'shop_order_id': data.get('shop_order_id')
        }
        form['sign'] = get_sign(form)
        return render_template('pay.html', url=url, method='GET', form=form)


class USDPayProcessor(object):

    @staticmethod
    def process_data(data):
        url = "https://core.piastrix.com/bill/create"
        headers = {'Content-Type': 'application/json'}
        request_data = {
            'payer_currency': data.get('currency'),
            'shop_amount': data.get('amount'),
            'shop_currency': data.get('currency'),
            'shop_id': Config.SHOP_ID,
            'shop_order_id': data.get('shop_order_id'),
        }
        request_data['sign'] = get_sign(request_data)

        try:
            response = requests.post(url=url, headers=headers, data=json.dumps(request_data))
        except Exception as e:
            log = LogModel(data)
            log.result = 'Exception'
            log.message = str(e)
            log.save_to_db()

            return render_template('error.html', error=Errors.INTERNAL_ERROR)

        log = LogModel(data)
        log.status_code = response.status_code

        if response.status_code == 200:
            response_data = response.json()

            if response_data.get('data') and response_data.get('data').get('id'):
                log.payment_id = response_data['data']['id']

            log.result = response_data['result']
            log.error = response_data['error_code']
            log.message = response_data['message']
            try:
                log.save_to_db()
            except Exception as e:
                current_app.logger.error(Errors.DB_ERROR, str(e))
                return render_template('error.html', error=Errors.INTERNAL_ERROR)

            if response_data.get('result'):
                redirect_url = response_data['data'].get('url')
                return redirect(redirect_url)
            else:

                return render_template('error.html', error=Errors.EXTERNAL_ERROR)
        else:
            log.result = 'Bad status code'
            log.message = response.text
            try:
                log.save_to_db()
            except Exception as e:
                current_app.logger.error(Errors.DB_ERROR, str(e))
                return render_template('error.html', error=Errors.INTERNAL_ERROR)

            return render_template('error.html', error=Errors.EXTERNAL_ERROR)


class RUBPayProcessor(object):

    @staticmethod
    def process_data(data):
        url = "https://core.piastrix.com/invoice/create"
        headers = {'Content-Type': 'application/json'}
        request_data = {
            'amount': data.get('amount'),
            'currency': data.get('currency'),
            'payway': 'payeer_rub',
            'shop_id': Config.SHOP_ID,
            'shop_order_id': data.get('shop_order_id'),
        }
        request_data['sign'] = get_sign(request_data)

        try:
            response = requests.post(url=url, headers=headers, data=json.dumps(request_data))
        except Exception as e:
            log = LogModel(data)
            log.result = 'Exception'
            log.message = str(e)
            log.save_to_db()

            return render_template('error.html', error=Errors.INTERNAL_ERROR)

        log = LogModel(data)
        log.status_code = response.status_code

        if response.status_code == 200:
            response_data = response.json()

            if response_data.get('data') and response_data.get('data').get('id'):
                log.payment_id = response_data['data']['id']

            log.result = response_data['result']
            log.error = response_data['error_code']
            log.message = response_data['message']
            try:
                log.save_to_db()
            except Exception as e:
                current_app.logger.error(Errors.DB_ERROR, str(e))
                return render_template('error.html', error=Errors.INTERNAL_ERROR)

            if response_data.get('result'):
                redirect_url = response_data['data']['url']
                method = response_data['data']['method']
                form = {
                    "lang": response_data['data']['data']['lang'],
                    "m_curorderid": response_data['data']['data']['m_curorderid'],
                    "m_historyid": response_data['data']['data']['m_historyid'],
                    "m_historytm": response_data['data']['data']['m_historytm'],
                    "referer": response_data['data']['data']['referer'],
                }
                return render_template('pay.html', url=redirect_url, method=method, form=form)
            else:
                return render_template('error.html', error=Errors.EXTERNAL_ERROR)
        else:
            log.result = 'Bad status code'
            log.message = response.text
            try:
                log.save_to_db()
            except Exception as e:
                current_app.logger.error(Errors.DB_ERROR, str(e))
                return render_template('error.html', error=Errors.INTERNAL_ERROR)

            return render_template('error.html', error=Errors.EXTERNAL_ERROR)
